﻿using BimAndCo.Utilities.Helpers;
using FnacDarty.JobInterview.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FnacDarty.JobInterview.Services.Stock
{
    public class StockService : IStockService
    {
        public StockService()
        {
        }

        /// <summary>
        ///     Vérifie si un produit est dans le stock
        /// </summary>
        /// <param name="stock">Stock à vérifier</param>
        /// <param name="EAN">Code du produit à retrouver</param>
        public bool HasProduct(JobInterview.Stock.Stock stock, string EAN)
        {
            if(stock == null)
            {
                throw new System.Exception("Stock is null");
            }
            if(EAN == null || EAN.Trim().Length != 8 || string.IsNullOrEmpty(EAN))
            {
                throw new System.Exception("EAN is in wrong format");
            }

            return stock.Products.Any(p => p.EAN == EAN);
        }

        /// <summary>
        ///     Récupère un produit du stock (retourne null si le produit n'est pas présent)
        /// </summary>
        /// <param name="stock">Stock à vérifier</param>
        /// <param name="EAN">Code du produit à récupérer</param>
        public Product GetProduct(JobInterview.Stock.Stock stock, string EAN)
        {
            bool hasProduct = HasProduct(stock, EAN);

            Product product = null;
            if (hasProduct)
            {
                product = stock.Products.FirstOrDefault(p => p.EAN == EAN);
            } 

            return product;
        }

        /// <summary>
        ///     Récupère la liste des produits actuellement en stock
        /// </summary>
        /// <param name="stock">Stock à vérifier</param>
        public IList<Product> GetCurrentProductsInStock(JobInterview.Stock.Stock stock)
        {
            List<Product> products = new List<Product>();
            foreach (Product product in stock.Products)
            {
                if (CurrentCountForProduct(stock, product) > 0)
                {
                    products.Add(product);
                }
            }
            return products;
        }

        /// <summary>
        ///     Récupère la liste des produits en stock à une date donnée
        /// </summary>
        /// <param name="stock">Stock à vérifier</param>
        /// <param name="date">Date à laquelle on veut connaitre les produit en stock</param>
        public IList<Product> GetProductsInStock(JobInterview.Stock.Stock stock, DateTime date)
        {
            List<Product> products = new List<Product>();
            foreach (Product product in stock.Products)
            {
                if (CountForProductAndDate(stock, product, null, date.Date) > 0)
                {
                    products.Add(product);
                }
            }
            return products;
        }

        /// <summary>
        ///     Récupère la liste des flux d'un produit dans un intervalle de dates
        /// </summary>
        /// <param name="p">Produit dont on veut vérifier les flux</param>
        /// <param name="startDate">Date à partir de laquelle on veut connaitre les flux (peut être nulle)</param>
        /// <param name="endDate">Date jusqu'à laquelle on veut connaitre les flux (peut être nulle)</param>
        public IEnumerable<FlowUnity> GetHistory(Product p, DateTime? startDate, DateTime? endDate)
        {
            Expression<Func<FlowUnity, bool>> expr = h => true;
            if (startDate != null)
            {
                expr = expr.And(h => h.Date >= startDate.Value.Date);
            }
            if (endDate != null)
            {
                expr = expr.And(h => h.Date <= endDate.Value.Date);
            }

            return p.History.Where(expr.Compile());
        }

        /// <summary>
        ///    Ajout d'un produit dans le stock
        /// </summary>
        /// <param name="stock">Stock dans lequel on veut ajouter le produit</param>
        /// <param name="p">Produit qu'on veut ajouter au stock</param>
        public void AddProduct(JobInterview.Stock.Stock stock, Product p)
        {
            if(p != null)
            {
                stock.Products.Add(p);
            }
        }

        /// <summary>
        ///    Ajout d'un flux à un produit du stock
        /// </summary>
        /// <param name="stock">Stock dans lequel on veut ajouter le flux d'un produit</param>
        /// <param name="flowUnity">Flux qu'on veut ajouter à un produit du stock</param>
        public void AddFlowUnity(FnacDarty.JobInterview.Stock.Stock stock, FlowUnity flowUnity)
        {
            if (flowUnity == null)
            {
                throw new System.Exception("flowUnity is null");
            }

            if (flowUnity.Product == null)
            {
                throw new System.Exception("flowUnity's product is null");
            }

            Product p = GetProduct(stock, flowUnity.Product.EAN);

            if(p == null)
            {
                p = flowUnity.Product;
                AddProduct(stock, p);
            }

            if (p.History.Any(h=>h.Type == FlowUnityType.Inventory))
            {
                FlowUnity lastInventory = p.History.OrderByDescending(h => h.Date).FirstOrDefault(h => h.Type == FlowUnityType.Inventory);

                if (lastInventory != null && flowUnity.Date <= lastInventory.Date)
                {
                    throw new Exception("It's not possible to add Flow with anterior date to last inventory");
                }
            }

            if (flowUnity != null && flowUnity.Type == FlowUnityType.Inventory && flowUnity.Quantity < 0)
            {
                throw new Exception("In inventory, it's not possible to add negative value");
            }

            p.History.Add(flowUnity);
        }

        /// <summary>
        ///    Ajout d'un flux de type inventaire à un produit du stock
        /// </summary>
        /// <param name="stock">Stock dans lequel on veut ajouter le flux d'un produit</param>
        /// <param name="flowUnity">Flux de type inventaire qu'on veut ajouter à un produit du stock</param>
        public void AddFlowUnityInventory(FnacDarty.JobInterview.Stock.Stock stock, FlowUnity flowUnity)
        {
            if(flowUnity != null && flowUnity.Type != FlowUnityType.Inventory)
            {
                flowUnity.Type = FlowUnityType.Inventory;
            }

            AddFlowUnity(stock, flowUnity);
        }

        /// <summary>
        ///    Ajout d'un ensemble de flux au stock
        /// </summary>
        /// <param name="stock">Stock dans lequel on veut ajouter le flux d'un produit</param>
        /// <param name="flow">Ensemble de flux</param>
        public void AddFlow(FnacDarty.JobInterview.Stock.Stock stock, Flow flow)
        {
            if (stock == null)
            {
                throw new System.Exception("flow is null");
            }
            if (flow == null)
            {
                throw new System.Exception("flow is null");
            }

            foreach(FlowUnity f in flow.Sets)
            {
                //Force la date (sans les heures) et le libellé des flux unitaire à ceux du flux global
                f.Date = flow.Date.Date;
                f.Name = flow.Name;
                
                AddFlowUnity(stock, f);
            }

        }

        /// <summary>
        ///    Calcul des stocks d'un produit pour un intervalle de dates
        /// </summary>
        /// <param name="stock">Stock ciblé</param>
        /// <param name="p">Produit dont on veut connaitre au stock</param>
        /// <param name="startDate">Date à partir de laquelle on veut connaitre les flux (peut être nulle)</param>
        /// <param name="endDate">Date jusqu'à laquelle on veut connaitre les flux (peut être nulle)</param>
        public int CountForProductAndDate(FnacDarty.JobInterview.Stock.Stock stock, Product p, DateTime? startDate, DateTime? endDate)
        {
            if (p == null)
            {
                throw new System.Exception("p is null");
            }

            Product product = GetProduct(stock, p.EAN);

            IEnumerable<FlowUnity> history = GetHistory(product, startDate?.Date, endDate?.Date);

            int count = 0;
            foreach(FlowUnity flowUnity in history)
            {
                if(flowUnity.Type == FlowUnityType.Classic)
                {
                    count += flowUnity.Quantity;
                }
                else
                {
                    count = flowUnity.Quantity;
                }
            }
            return count;
        }

        /// <summary>
        ///    Calcul des variations de stock d'un produit pour un intervalle de dates
        /// </summary>
        /// <param name="stock">Stock ciblé</param>
        /// <param name="p">Produit dont on veut connaitre au stock</param>
        /// <param name="d1">Date à partir de laquelle on veut connaitre la variation (peut être nulle)</param>
        /// <param name="d2">Date jusqu'à laquelle on veut connaitre la variation (peut être nulle)</param>
        public int CountVariation(FnacDarty.JobInterview.Stock.Stock stock, Product p, DateTime? d1, DateTime? d2)
        {
            if (p == null)
            {
                throw new System.Exception("p is null");
            }

            Product product = GetProduct(stock, p.EAN);

            int CountD1 = CountForProductAndDate(stock, product, null, d1?.Date);
            int CountD2 = CountForProductAndDate(stock, product, null, d2?.Date);

            return CountD2 - CountD1;
        }

        /// <summary>
        ///    Calcul du stock actuel d'un produit
        /// </summary>
        /// <param name="stock">Stock ciblé</param>
        /// <param name="p">Produit dont on veut connaitre au stock</param>
        public int CurrentCountForProduct(FnacDarty.JobInterview.Stock.Stock stock, Product p)
        {
            return CountForProductAndDate(stock, p, null, DateTime.Now.Date);
        }

        /// <summary>
        ///    Calcul du stock total pour une date donnée
        /// </summary>
        /// <param name="stock">Stock ciblé</param>
        /// <param name="date">Date à laquelle on veut connaitre le stock</param>
        public int TotalForDate(FnacDarty.JobInterview.Stock.Stock stock, DateTime date)
        {
            List<Product> products = GetProductsInStock(stock, date.Date).ToList();

            int count = 0;
            foreach (Product product in products)
            {
                count += CountForProductAndDate(stock, product, null, date.Date);
            }
            return count;
        }

        /// <summary>
        ///    Calcul du stock total actuel
        /// </summary>
        /// <param name="stock">Stock ciblé</param>
        public int CurrentTotal(FnacDarty.JobInterview.Stock.Stock stock)
        {
            List<Product> products = GetCurrentProductsInStock(stock).ToList();

            int count = 0;
            foreach (Product product in products)
            {
                count += CurrentCountForProduct(stock, product);
            }
            return count;
        }
    }
}