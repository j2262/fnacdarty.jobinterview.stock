﻿using FnacDarty.JobInterview.Stock;
using System;
using System.Collections.Generic;

namespace FnacDarty.JobInterview.Services.Stock
{
    public interface IStockService : IService
    {
        /// <summary>
        ///     Vérifie si un produit est dans le stock
        /// </summary>
        /// <param name="stock">Stock à vérifier</param>
        /// <param name="EAN">Code du produit à retrouver</param>
        bool HasProduct(FnacDarty.JobInterview.Stock.Stock stock, string EAN);

        /// <summary>
        ///     Récupère un produit du stock (retourne null si le produit n'est pas présent)
        /// </summary>
        /// <param name="stock">Stock à vérifier</param>
        /// <param name="EAN">Code du produit à récupérer</param>
        Product GetProduct(FnacDarty.JobInterview.Stock.Stock stock, string EAN);

        /// <summary>
        ///     Récupère la liste des produits actuellement en stock
        /// </summary>
        /// <param name="stock">Stock à vérifier</param>
        IList<Product> GetCurrentProductsInStock(JobInterview.Stock.Stock stock);

        /// <summary>
        ///     Récupère la liste des produits en stock à une date donnée
        /// </summary>
        /// <param name="stock">Stock à vérifier</param>
        /// <param name="date">Date à laquelle on veut connaitre les produit en stock</param>
        IList<Product> GetProductsInStock(JobInterview.Stock.Stock stock, DateTime date);

        /// <summary>
        ///     Récupère la liste des flux d'un produit dans un intervalle de dates
        /// </summary>
        /// <param name="p">Produit dont on veut vérifier les flux</param>
        /// <param name="startDate">Date à partir de laquelle on veut connaitre les flux (peut être nulle)</param>
        /// <param name="endDate">Date jusqu'à laquelle on veut connaitre les flux (peut être nulle)</param>
        IEnumerable<FlowUnity> GetHistory(Product p, DateTime? startDate, DateTime? endDate);

        /// <summary>
        ///    Ajout d'un produit dans le stock
        /// </summary>
        /// <param name="stock">Stock dans lequel on veut ajouter le produit</param>
        /// <param name="p">Produit qu'on veut ajouter au stock</param>
        void AddProduct(FnacDarty.JobInterview.Stock.Stock stock, Product p);

        /// <summary>
        ///    Ajout d'un flux à un produit du stock
        /// </summary>
        /// <param name="stock">Stock dans lequel on veut ajouter le flux d'un produit</param>
        /// <param name="flowUnity">Flux qu'on veut ajouter à un produit du stock</param>
        void AddFlowUnity(FnacDarty.JobInterview.Stock.Stock stock, FlowUnity flowUnity);

        /// <summary>
        ///    Ajout d'un flux de type inventaire à un produit du stock
        /// </summary>
        /// <param name="stock">Stock dans lequel on veut ajouter le flux d'un produit</param>
        /// <param name="flowUnity">Flux de type inventaire qu'on veut ajouter à un produit du stock</param>
        void AddFlowUnityInventory(FnacDarty.JobInterview.Stock.Stock stock, FlowUnity flowUnity);

        /// <summary>
        ///    Ajout d'un ensemble de flux au stock
        /// </summary>
        /// <param name="stock">Stock dans lequel on veut ajouter le flux d'un produit</param>
        /// <param name="flow">Ensemble de flux</param>
        void AddFlow(FnacDarty.JobInterview.Stock.Stock stock, Flow flow);

        /// <summary>
        ///    Calcul des stocks d'un produit pour un intervalle de dates
        /// </summary>
        /// <param name="stock">Stock ciblé</param>
        /// <param name="p">Produit dont on veut connaitre au stock</param>
        /// <param name="startDate">Date à partir de laquelle on veut connaitre les flux (peut être nulle)</param>
        /// <param name="endDate">Date jusqu'à laquelle on veut connaitre les flux (peut être nulle)</param>
        int CountForProductAndDate(FnacDarty.JobInterview.Stock.Stock stock, Product p, DateTime? startDate, DateTime? endDate);

        /// <summary>
        ///    Calcul des variations de stock d'un produit pour un intervalle de dates
        /// </summary>
        /// <param name="stock">Stock ciblé</param>
        /// <param name="p">Produit dont on veut connaitre au stock</param>
        /// <param name="d1">Date à partir de laquelle on veut connaitre la variation (peut être nulle)</param>
        /// <param name="d2">Date jusqu'à laquelle on veut connaitre la variation (peut être nulle)</param>
        int CountVariation(FnacDarty.JobInterview.Stock.Stock stock, Product p, DateTime? d1, DateTime? d2);

        /// <summary>
        ///    Calcul du stock actuel d'un produit
        /// </summary>
        /// <param name="stock">Stock ciblé</param>
        /// <param name="p">Produit dont on veut connaitre au stock</param>
        int CurrentCountForProduct(FnacDarty.JobInterview.Stock.Stock stock, Product p);

        /// <summary>
        ///    Calcul du stock total pour une date donnée
        /// </summary>
        /// <param name="stock">Stock ciblé</param>
        /// <param name="date">Date à laquelle on veut connaitre le stock</param>
        int TotalForDate(FnacDarty.JobInterview.Stock.Stock stock, DateTime date);

        /// <summary>
        ///    Calcul du stock total actuel
        /// </summary>
        /// <param name="stock">Stock ciblé</param>
        int CurrentTotal(FnacDarty.JobInterview.Stock.Stock stock);
    }
}
