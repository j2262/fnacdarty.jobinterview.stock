using FnacDarty.JobInterview.Services.Stock;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDarty.JobInterview.Stock.UnitTest
{
    [TestFixture]
    public class StockTest
    {
        private IStockService _stockService;
        private Stock _stock;
        private Product _p1;
        private Product _p2;
        private Product _p3;
        private Product _p4;
        private Product _p99;

        private FlowUnity _flowUnityP1Set1;
        private FlowUnity _flowUnityP1Set2;
        private FlowUnity _flowUnityP1Set3;
        private FlowUnity _flowUnityP2Set1;
        private FlowUnity _flowUnityP2Set2;
        private FlowUnity _flowUnityP2Set3;
        private FlowUnity _flowUnityP2Set4;
        private FlowUnity _flowUnityP3Set1;
        private FlowUnity _flowUnityP3Set2;

        #region Setup
        [SetUp]
        public void Initialize()
        {
            _stockService = new StockService();
            _stock = new Stock();

            List<Product> products = new List<Product>();

            _p1 = new Product()
            {
                Name = "Product 1",
                EAN = "EAN00001",
            };

            _flowUnityP1Set1 = new FlowUnity()
            {
                Date = new System.DateTime(2020, 01, 01),
                Product = _p1,
                Quantity = 10,
                Name = "Achat N�1",
                Type = FlowUnityType.Classic
            };
            _flowUnityP1Set2 = new FlowUnity()
            {
                Date = new System.DateTime(2020, 01, 02),
                Product = _p1,
                Quantity = -3,
                Name = "Cmd N�1",
                Type = FlowUnityType.Classic
            };
            _flowUnityP1Set3 = new FlowUnity()
            {
                Date = new System.DateTime(2020, 01, 03),
                Product = _p1,
                Quantity = -1,
                Name = "Cmd N�2",
                Type = FlowUnityType.Classic
            };
            List<FlowUnity> p1FlowUnity = new List<FlowUnity>()
            {
                _flowUnityP1Set1,
                _flowUnityP1Set2,
                _flowUnityP1Set3,
            };
            _p1.History = p1FlowUnity;
            products.Add(_p1);

            _p2 = new Product()
            {
                Name = "Product 2",
                EAN = "EAN00002",
            };

            _flowUnityP2Set1 = new FlowUnity()
            {
                Date = new System.DateTime(2020, 01, 01),
                Product = _p2,
                Quantity = 10,
                Name = "Achat N�2",
                Type = FlowUnityType.Classic
            };
            _flowUnityP2Set2 = new FlowUnity()
            {
                Date = new System.DateTime(2020, 01, 02),
                Product = _p2,
                Quantity = -3,
                Name = "Cmd N�1",
                Type = FlowUnityType.Classic
            };
            _flowUnityP2Set3 = new FlowUnity()
            {
                Date = new System.DateTime(2020, 01, 03),
                Product = _p2,
                Quantity = -10,
                Name = "Cmd N�2",
                Type = FlowUnityType.Classic
            };
            _flowUnityP2Set4 = new FlowUnity()
            {
                Date = new System.DateTime(2020, 01, 04),
                Product = _p2,
                Quantity = 1,
                Name = "Inventaire",
                Type = FlowUnityType.Inventory
            };
            List<FlowUnity> p2FlowUnity = new List<FlowUnity>()
            {
                _flowUnityP2Set1,
                _flowUnityP2Set2,
                _flowUnityP2Set3,
                _flowUnityP2Set4,
            };
            _p2.History = p2FlowUnity;
            products.Add(_p2);

            _p3 = new Product()
            {
                Name = "Product 3",
                EAN = "EAN00003",
            };

            _flowUnityP3Set1 = new FlowUnity()
            {
                Date = new System.DateTime(2020, 01, 01),
                Product = _p3,
                Quantity = 10,
                Name = "Achat N�3",
                Type = FlowUnityType.Classic
            };
            _flowUnityP3Set2 = new FlowUnity()
            {
                Date = new System.DateTime(2020, 01, 02),
                Product = _p3,
                Quantity = -3,
                Name = "Cmd N�1",
                Type = FlowUnityType.Classic
            };
            List<FlowUnity> p3FlowUnity = new List<FlowUnity>()
            {
                _flowUnityP3Set1,
                _flowUnityP3Set2
            };
            _p3.History = p3FlowUnity;
            products.Add(_p3);

            _stock.Products = products;

            _p4 = new Product()
            {
                Name = "Product 4",
                EAN = "EAN00004",
            };

            _p99 = new Product()
            {
                Name = "Product 99",
                EAN = "EAN00099",
            };
            List<FlowUnity> p99FlowUnity = new List<FlowUnity>()
            {
                new FlowUnity()
                {
                    Date = new System.DateTime(2022,05,18),
                    Product = _p99,
                    Quantity = 10,
                    Name = "Test n�gatif 0",
                    Type = FlowUnityType.Classic
                },
                new FlowUnity()
                {
                    Date = new System.DateTime(2020,01,02),
                    Product = _p99,
                    Quantity = -15,
                    Name = "Test n�gatif 1",
                    Type = FlowUnityType.Classic
                }
            };
            _p99.History = p99FlowUnity;
            products.Add(_p99);
        }

        [TearDown]
        public void Clear()
        {
        }
        #endregion

        [Test]
        [TestCase("EAN00004", false)]
        [TestCase("EAN00001", true)]
        [TestCase("EAN00002", true)]
        [TestCase("EAN00003", true)]
        public void HasProductTest(string EAN, bool expectedExisted)
        {
            bool isExisted = _stockService.HasProduct(_stock, EAN);

            Assert.IsNotNull(_stock);
            Assert.AreEqual(expectedExisted, isExisted);
        }

        [Test]
        [TestCase("EAN00001")]
        [TestCase("EAN00002")]
        [TestCase("EAN00003")]
        [TestCase("EAN00005")]
        public void GetProductTest(string EAN)
        {
            Product expected = GetProduct(EAN);


            Product p = _stockService.GetProduct(_stock, EAN);

            Assert.IsNotNull(_stock);
            Assert.AreEqual(expected, p);
        }

        [Test]
        public void GetCurrentProductsInStockTest()
        {
            List<Product> products = _stockService.GetCurrentProductsInStock(_stock).ToList();

            Assert.IsNotNull(_stock);
            Assert.IsNotNull(products);
            Assert.IsTrue(products.Any());
            Assert.AreEqual(3, products.Count);
            Assert.IsTrue(products.Any(p => p.EAN == "EAN00001"));
            Assert.IsTrue(products.Any(p => p.EAN == "EAN00002"));
            Assert.IsTrue(products.Any(p => p.EAN == "EAN00003"));
            Assert.IsTrue(!products.Any(p => p.EAN == "EAN00099"));
        }

        [Test]
        [TestCase(2020, 01, 01, 3)]
        [TestCase(2020, 01, 02, 3)]
        [TestCase(2020, 01, 03, 2)]
        [TestCase(2020, 01, 04, 3)]
        public void GetProductsInStockTest(int year, int month, int day, int expectedCount)
        {
            DateTime date = new DateTime(year, month, day);

            List<Product> products = _stockService.GetProductsInStock(_stock, date).ToList();

            Assert.IsNotNull(_stock);
            Assert.IsNotNull(products);
            Assert.IsTrue(products.Any());
            Assert.AreEqual(expectedCount, products.Count);
        }

        [Test]
        [TestCase("EAN00001", 4)]
        [TestCase("EAN00002", 4)]
        [TestCase("EAN00003", 4)]
        [TestCase("EAN00004", 5)]
        public void AddProductTest(string EAN, int expectedProductsCount)
        {
            Product p = _stockService.GetProduct(_stock, EAN);

            if (EAN == "EAN00004")
            {
                p = _p4;
            }
            else
            {
                Assert.IsNotNull(p);
                p = null;
            }

            _stockService.AddProduct(_stock, p);

            Assert.IsNotNull(_stock);
            Assert.AreEqual(expectedProductsCount, _stock.Products.Count);
        }

        [Test]
        [TestCase("EAN00004", 10)]
        [TestCase("EAN00001", 10)]
        [TestCase("EAN00003", -2)]
        public void AddFlowUnityTest(string EAN, int quantity)
        {
            Product p = GetProduct(EAN);
            Assert.IsNotNull(p);

            FlowUnity flowUinty = new FlowUnity()
            {
                Date = new System.DateTime(2022, 05, 18),
                Product = p,
                Quantity = quantity,
                Name = "Test",
                Type = FlowUnityType.Classic
            };

            _stockService.AddFlowUnity(_stock, flowUinty);

            Assert.IsNotNull(_stock);
            Assert.IsNotNull(flowUinty);

            Product pResult = _stock.Products.FirstOrDefault(p => p.EAN == EAN);
            Assert.IsNotNull(pResult);

            bool anyResult = pResult.History.Any(h => h.Date == new System.DateTime(2022, 05, 18) && h.Quantity == quantity && h.Type == FlowUnityType.Classic);
            Assert.IsTrue(anyResult);
        }

        [Test]
        [TestCase("EAN00002", 10)]
        public void AddFlowUnity_NotPossibleTest(string EAN, int quantity)
        {
            Product p = GetProduct(EAN);
            Assert.IsNotNull(p);

            FlowUnity flowUinty = new FlowUnity()
            {
                Date = new System.DateTime(2020, 01, 02),
                Product = p,
                Quantity = quantity,
                Name = "Test",
                Type = FlowUnityType.Classic
            };

            Exception ex = Assert.Throws<Exception>(() => _stockService.AddFlowUnity(_stock, flowUinty));

            Assert.AreEqual("It's not possible to add Flow with anterior date to last inventory", ex.Message);

        }

        [Test]
        [TestCase("EAN00004", 2021, 01, 01, 100)]
        [TestCase("EAN00001", 2021, 01, 01, 101)]
        [TestCase("EAN00003", 2021, 01, 01, 102)]
        [TestCase("EAN00002", 2021, 01, 01, 22)]
        [TestCase("EAN00004", 2020, 01, 02, 100)]
        [TestCase("EAN00001", 2020, 01, 02, 101)]
        [TestCase("EAN00003", 2020, 01, 02, 102)]
        public void AddFlowUnityInventoryTest(string EAN, int year, int month, int day, int quantity)
        {
            Product p = GetProduct(EAN);
            Assert.IsNotNull(p);

            FlowUnity flowUinty = new FlowUnity()
            {
                Date = new System.DateTime(year, month, day),
                Product = p,
                Quantity = quantity,
                Name = "Test",
                Type = FlowUnityType.Classic
            };

            _stockService.AddFlowUnityInventory(_stock, flowUinty);

            Assert.IsNotNull(_stock);
            Assert.IsNotNull(flowUinty);

            Product pResult = _stock.Products.FirstOrDefault(p => p.EAN == EAN);
            Assert.IsNotNull(pResult);

            bool anyResult = pResult.History.Any(h => h.Date == new System.DateTime(year, month, day) && h.Quantity == quantity && h.Type == FlowUnityType.Inventory);
            Assert.IsTrue(anyResult);
        }

        [Test]
        [TestCase("EAN00002", 100)]
        public void AddFlowUnityInventory_NotPossibleTest(string EAN, int quantity)
        {
            Product p = GetProduct(EAN);
            Assert.IsNotNull(p);

            FlowUnity flowUinty = new FlowUnity()
            {
                Date = new System.DateTime(2020, 01, 02),
                Product = p,
                Quantity = quantity,
                Name = "Test",
                Type = FlowUnityType.Inventory
            };

            Exception ex = Assert.Throws<Exception>(() => _stockService.AddFlowUnityInventory(_stock, flowUinty));

            Assert.AreEqual("It's not possible to add Flow with anterior date to last inventory", ex.Message);
        }

        [Test]
        [TestCase("EAN00002", -100)]
        public void AddFlowUnityInventory_NegatuveQuantityTest(string EAN, int quantity)
        {
            Product p = GetProduct(EAN);
            Assert.IsNotNull(p);

            FlowUnity flowUinty = new FlowUnity()
            {
                Date = new System.DateTime(2021, 01, 02),
                Product = p,
                Quantity = quantity,
                Name = "Test",
                Type = FlowUnityType.Inventory
            };

            Exception ex = Assert.Throws<Exception>(() => _stockService.AddFlowUnityInventory(_stock, flowUinty));

            Assert.AreEqual("In inventory, it's not possible to add negative value", ex.Message);
        }

        [Test]
        [TestCase(1, 2020, 01, 01, "Achat N�1")]
        [TestCase(2, 2020, 01, 02, "Cmd N�1")]
        [TestCase(3, 2020, 01, 03, "Cmd N�2")]
        [TestCase(4, 2020, 01, 04, "inventaire")]
        public void AddFlow(int setType, int year, int month, int day, string name)
        {
            _stock = new Stock();
            List<FlowUnity> set = new List<FlowUnity>();
            switch(setType)
            {
                case 1: 
                    _flowUnityP1Set1.Product.History = new List<FlowUnity>();
                    _flowUnityP2Set1.Product.History = new List<FlowUnity>();
                    _flowUnityP3Set1.Product.History = new List<FlowUnity>();
                    set = new List<FlowUnity>() { _flowUnityP1Set1, _flowUnityP2Set1, _flowUnityP3Set1 };
                    break;
                case 2:
                    _flowUnityP1Set2.Product.History = new List<FlowUnity>();
                    _flowUnityP2Set2.Product.History = new List<FlowUnity>();
                    _flowUnityP3Set2.Product.History = new List<FlowUnity>();
                    set = new List<FlowUnity>() { _flowUnityP1Set2, _flowUnityP2Set2, _flowUnityP3Set2 };
                    break;
                case 3:
                    _flowUnityP1Set3.Product.History = new List<FlowUnity>();
                    _flowUnityP2Set3.Product.History = new List<FlowUnity>();
                    set = new List<FlowUnity>() { _flowUnityP1Set3, _flowUnityP2Set3 };
                    break;
                case 4:
                    _flowUnityP2Set4.Product.History = new List<FlowUnity>();
                    set = new List<FlowUnity>() { _flowUnityP2Set4 };
                    break;
            }
            Assert.IsNotNull(_stock);

            DateTime date = new System.DateTime(year, month, day).Date;
            Flow flow = new Flow() { Date = date, Name = name, Sets = set };
            Assert.IsNotNull(flow);
            
            _stockService.AddFlow(_stock, flow);

            foreach(FlowUnity f in set)
            {
                Product product = _stock.Products.FirstOrDefault(p => p.EAN == f.Product.EAN);
                Assert.IsNotNull(product);
                Assert.IsTrue(product.History.Any(h=>h.Date == date && h.Name == name));
            }
        }

        [Test]
        [TestCase("EAN00001", 2020, 01, 01, 10)]
        [TestCase("EAN00001", 2020, 01, 02, 7)]
        [TestCase("EAN00001", 2020, 01, 03, 6)]
        [TestCase("EAN00002", 2020, 01, 01, 10)]
        [TestCase("EAN00002", 2020, 01, 02, 7)]
        [TestCase("EAN00002", 2020, 01, 03, -3)]
        [TestCase("EAN00002", 2020, 01, 04, 1)]
        [TestCase("EAN00003", 2020, 01, 01, 10)]
        [TestCase("EAN00003", 2020, 01, 03, 7)]
        public void CountForProductAndDateTest(string EAN, int year, int month, int day, int expectedCount)
        {
            Product p = GetProduct(EAN);
            Assert.IsNotNull(p);
            Assert.IsNotNull(_stock);

            DateTime date = new System.DateTime(year, month, day);

            int countResult = _stockService.CountForProductAndDate(_stock, p, null, date);
            Assert.AreEqual(expectedCount, countResult);
        }

        [Test]
        [TestCase("EAN00004", 10, 10)]
        [TestCase("EAN00001", 10, 16)]
        [TestCase("EAN00003", -2, 5)]
        [TestCase("EAN00002", 1, 2)]
        public void CountForProductAndDateTest2(string EAN, int quantity, int expectedCount)
        {
            Product p = null;
            switch (EAN)
            {
                case "EAN00001": p = _p1; break;
                case "EAN00002": p = _p2; break;
                case "EAN00003": p = _p3; break;
                case "EAN00004": p = _p4; break;
            }
            Assert.IsNotNull(p);

            FlowUnity flowUinty = new FlowUnity()
            {
                Date = new System.DateTime(2022, 05, 18),
                Product = p,
                Quantity = quantity,
                Name = "Test",
                Type = FlowUnityType.Classic
            };

            _stockService.AddFlowUnity(_stock, flowUinty);

            Assert.IsNotNull(_stock);
            Assert.IsNotNull(flowUinty);

            Product pResult = _stock.Products.FirstOrDefault(p => p.EAN == EAN);
            Assert.IsNotNull(pResult);

            int countResult = _stockService.CountForProductAndDate(_stock, pResult, null, new System.DateTime(2022, 05, 18));
            Assert.AreEqual(expectedCount, countResult);
        }

        [Test]
        [TestCase("EAN00001", 2020, 01, 01, 2020, 01, 02, -3)]
        [TestCase("EAN00001", 2020, 01, 01, 2020, 01, 03, -4)]
        [TestCase("EAN00002", 2020, 01, 01, 2020, 01, 03, -13)]
        [TestCase("EAN00002", 2020, 01, 01, 2020, 01, 04, -9)]
        [TestCase("EAN00002", 2020, 01, 03, 2020, 01, 04, 4)]
        public void CountVariationTest(string EAN, int year1, int month1, int day1, int year2, int month2, int day2, int expectedCount)
        {
            Product p = GetProduct(EAN);
            Assert.IsNotNull(p);
            Assert.IsNotNull(_stock);

            DateTime date1 = new System.DateTime(year1, month1, day1);
            DateTime date2 = new System.DateTime(year2, month2, day2);

            int countResult = _stockService.CountVariation(_stock, p, date1, date2);

            Assert.AreEqual(expectedCount, countResult);
        }

        [Test]
        [TestCase("EAN00001", 6)]
        [TestCase("EAN00002", 1)]
        [TestCase("EAN00003", 7)]
        public void CurrentCountForProductTest(string EAN, int expectedCount)
        {
            Product p = GetProduct(EAN);
            Assert.IsNotNull(p);
            Assert.IsNotNull(_stock);

            int countResult = _stockService.CurrentCountForProduct(_stock, p);

            Assert.AreEqual(expectedCount, countResult);
        }

        [Test]
        [TestCase(2020, 01, 01, 30)]
        [TestCase(2020, 01, 02, 21)]
        [TestCase(2020, 01, 03, 13)]
        [TestCase(2020, 01, 04, 14)]
        public void TotalForDateTest(int year, int month, int day, int expectedCount)
        {
            DateTime date = new DateTime(year, month, day);

            int countResult = _stockService.TotalForDate(_stock, date);

            Assert.IsNotNull(_stock);
            Assert.AreEqual(expectedCount, countResult);
        }

        [Test]
        public void CurrentTotalTest()
        {
            int count = _stockService.CurrentTotal(_stock);

            Assert.IsNotNull(_stock);
            Assert.AreEqual(14, count);
        }

        private Product GetProduct(string EAN)
        {
            Product p = null;
            switch (EAN)
            {
                case "EAN00001": p = _p1; break;
                case "EAN00002": p = _p2; break;
                case "EAN00003": p = _p3; break;
                case "EAN00004": p = _p4; break;
            }
            return p;
        }
    }
}
