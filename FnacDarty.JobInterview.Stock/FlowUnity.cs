﻿using System;
using System.Collections.Generic;

namespace FnacDarty.JobInterview.Stock
{
    public enum FlowUnityType
    {
        Classic = 0,
        Inventory = 1
    }

    public class FlowUnity
    {
        public Guid Guid { get; set; }

        public DateTime Date { get; set; }

        public string Name { get; set; }

        public Product Product { get; set; }

        public int Quantity { get; set; }

        public FlowUnityType Type { get; set; }
    }
}
