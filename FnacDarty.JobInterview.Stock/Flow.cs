﻿using System;
using System.Collections.Generic;

namespace FnacDarty.JobInterview.Stock
{
    public class Flow
    {
        public Guid Guid { get; set; }

        public DateTime Date { get; set; }

        public string Name { get; set; }

        public IList<FlowUnity> Sets { get; set;} 
    }
}
