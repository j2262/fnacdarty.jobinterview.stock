﻿using System;
using System.Collections.Generic;

namespace FnacDarty.JobInterview.Stock
{
    public class Product
    {
        public Guid Guid { get; set; }

        public string EAN { get; set; }

        public string Name { get; set; }

        public IList<FlowUnity> History { get; set; }

        public Product()
        {
            History = new List<FlowUnity>();
        }

    }
}
