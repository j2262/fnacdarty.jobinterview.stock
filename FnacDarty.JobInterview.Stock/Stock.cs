﻿using System;
using System.Collections.Generic;

namespace FnacDarty.JobInterview.Stock
{
    public class Stock
    {
        public Guid Guid { get; set; }

        public IList<Product> Products { get; set; }

        public Stock()
        {
            Products = new List<Product>();
        }
    }
}
